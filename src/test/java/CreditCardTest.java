import com.fasterxml.jackson.databind.ObjectMapper;
import com.jumia.pay.challengetest.ChallengeTestApplication;
import com.jumia.pay.challengetest.entity.CreditCardStat;
import com.jumia.pay.challengetest.repository.CreditCardStatRepository;
import com.jumia.pay.challengetest.service.CreditCardService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ChallengeTestApplication.class)
public class CreditCardTest {

    @Resource
    private CreditCardStatRepository creditCardStatRepo;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private ObjectMapper objectMappter;

    @Test
    public void save() {
        CreditCardStat stat = new CreditCardStat();
        stat.setCardNumber(12345678);
        stat.setCount(2);

        creditCardStatRepo.save(stat);

        CreditCardStat stat2 = creditCardStatRepo.findById(12345678L).get();
        Assert.assertEquals(2, stat2.getCount());
    }

    @Test
    public void testFindApi() throws IOException {
        String serviceResponse = "{\"success\":true,\"payload\":{\"scheme\":\"visa\",\"type\":\"debit\",\"bank\":\"Jyske Bank\"}}";
        Assert.assertEquals(objectMappter.writeValueAsString(creditCardService.findCreditCardInformation("45717360")), serviceResponse);
    }
}
