
package com.jumia.pay.challengetest.integration.obj;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "number",
    "scheme",
    "type",
    "brand",
    "prepaid",
    "country",
    "bank"
})
public class CreditCard implements Serializable
{

    @JsonProperty("number")
    private CreditCardId number;
    @JsonProperty("scheme")
    private String scheme;
    @JsonProperty("type")
    private String type;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("prepaid")
    private boolean prepaid;
    @JsonProperty("country")
    private Country country;
    @JsonProperty("bank")
    private Bank bank;
    private final static long serialVersionUID = 5067737558460635492L;

    @JsonProperty("number")
    public CreditCardId getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(CreditCardId number) {
        this.number = number;
    }

    @JsonProperty("scheme")
    public String getScheme() {
        return scheme;
    }

    @JsonProperty("scheme")
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("prepaid")
    public boolean isPrepaid() {
        return prepaid;
    }

    @JsonProperty("prepaid")
    public void setPrepaid(boolean prepaid) {
        this.prepaid = prepaid;
    }

    @JsonProperty("country")
    public Country getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(Country country) {
        this.country = country;
    }

    @JsonProperty("bank")
    public Bank getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
