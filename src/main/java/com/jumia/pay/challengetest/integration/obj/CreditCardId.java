
package com.jumia.pay.challengetest.integration.obj;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "length",
    "luhn"
})
public class CreditCardId implements Serializable
{

    @JsonProperty("length")
    private int length;
    @JsonProperty("luhn")
    private boolean luhn;
    private final static long serialVersionUID = 3656244580783534125L;

    @JsonProperty("length")
    public int getLength() {
        return length;
    }

    @JsonProperty("length")
    public void setLength(int length) {
        this.length = length;
    }

    @JsonProperty("luhn")
    public boolean isLuhn() {
        return luhn;
    }

    @JsonProperty("luhn")
    public void setLuhn(boolean luhn) {
        this.luhn = luhn;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
