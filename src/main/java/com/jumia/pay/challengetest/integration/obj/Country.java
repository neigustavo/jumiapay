
package com.jumia.pay.challengetest.integration.obj;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "numeric",
    "alpha2",
    "name",
    "emoji",
    "currency",
    "latitude",
    "longitude"
})
public class Country implements Serializable
{

    @JsonProperty("numeric")
    private String numeric;
    @JsonProperty("alpha2")
    private String alpha2;
    @JsonProperty("name")
    private String name;
    @JsonProperty("emoji")
    private String emoji;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("latitude")
    private int latitude;
    @JsonProperty("longitude")
    private int longitude;
    private final static long serialVersionUID = -2713299472059323300L;

    @JsonProperty("numeric")
    public String getNumeric() {
        return numeric;
    }

    @JsonProperty("numeric")
    public void setNumeric(String numeric) {
        this.numeric = numeric;
    }

    @JsonProperty("alpha2")
    public String getAlpha2() {
        return alpha2;
    }

    @JsonProperty("alpha2")
    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("emoji")
    public String getEmoji() {
        return emoji;
    }

    @JsonProperty("emoji")
    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("latitude")
    public int getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("longitude")
    public int getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
