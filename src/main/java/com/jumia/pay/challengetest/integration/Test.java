package com.jumia.pay.challengetest.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jumia.pay.challengetest.integration.obj.CreditCard;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Test {

    public static void main(String[] args) {
        HttpURLConnection connection = null;
        try {
            //Create connection
            URL url = new URL("https://lookup.binlist.net/"+ "4571736045222255");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            ObjectMapper mapper = new ObjectMapper();
            CreditCard value = mapper.readValue(response.toString(), CreditCard.class);
            System.out.println(value.toString());
            //return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
