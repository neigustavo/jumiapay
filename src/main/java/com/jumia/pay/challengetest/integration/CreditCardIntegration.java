package com.jumia.pay.challengetest.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jumia.pay.challengetest.integration.obj.CreditCard;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class CreditCardIntegration {

    @Value("${com.jumia.pay.integration.url}")
    private String integrationUrl;

    public CreditCard findCreditCardInformation(String cardId) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(findCreditCardInformationString(cardId), CreditCard.class);

    }

    private String findCreditCardInformationString(String cardId) throws IOException {
        HttpURLConnection conn = null;
        try {
            //Create conn
            URL url = new URL(integrationUrl+cardId);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            //Using cache improves perfomance
            conn.setUseCaches(true);

            //Get Response
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
            rd.close();
            return response.toString();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }
}
