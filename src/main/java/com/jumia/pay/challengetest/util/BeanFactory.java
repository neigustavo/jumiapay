package com.jumia.pay.challengetest.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class BeanFactory {

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }
}
