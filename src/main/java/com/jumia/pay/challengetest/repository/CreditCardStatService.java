package com.jumia.pay.challengetest.repository;

import com.jumia.pay.challengetest.entity.CreditCardStat;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class CreditCardStatService {

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    CreditCardStatRepository repository;


    public List<CreditCardStat> findAll(int offset, int limit) {
        TypedQuery<CreditCardStat> query = entityManager.createQuery("SELECT c FROM CreditCardStat c", CreditCardStat.class);
        query.setFirstResult(offset-1);
        query.setMaxResults(limit-1);
        return query.getResultList();
    }

}
