package com.jumia.pay.challengetest.repository;

import com.jumia.pay.challengetest.entity.CreditCardStat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditCardStatRepository extends JpaRepository<CreditCardStat, Long> {

}
