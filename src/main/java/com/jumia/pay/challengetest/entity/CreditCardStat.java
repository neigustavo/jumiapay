package com.jumia.pay.challengetest.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CreditCardStat {

    @Id
    private long cardNumber;

    private int count;

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
