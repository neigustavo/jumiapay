package com.jumia.pay.challengetest.entity;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

public class PageableStat implements Pageable, Serializable {


    private int limit;
    private long offset;
    private Sort sort;

    public PageableStat(long offset, int limit, Sort sort) {
        if (offset < 0) {
            throw new IllegalArgumentException("Offset index must not be less than zero!");
        }

        if (limit < 1) {
            throw new IllegalArgumentException("Limit must not be less than one!");
        }
        this.limit = limit;
        this.offset = offset;
        this.sort = sort;
    }

    public PageableStat(long offset, int limit, Sort.Direction direction, String... properties) {
        this(offset, limit, new Sort(direction, properties));
    }


    public PageableStat(long offset, int limit) {
        this(offset, limit, new Sort(Sort.Direction.DESC, "count"));
    }

    @Override
    public int getPageNumber() {
        return new Long(offset).intValue() / limit;
    }

    @Override
    public int getPageSize() {
        return limit;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public Pageable next() {
        return new PageableStat(getOffset() + getPageSize(), getPageSize(), getSort());
    }

    public PageableStat previous() {
        return hasPrevious() ? new PageableStat(getOffset() - getPageSize(), getPageSize(), getSort()) : this;
    }

    @Override
    public Pageable previousOrFirst() {
        return hasPrevious() ? previous() : first();
    }

    @Override
    public Pageable first() {
        return new PageableStat(0, getPageSize(), getSort());
    }

    @Override
    public boolean hasPrevious() {
        return offset > limit;
    }
}
