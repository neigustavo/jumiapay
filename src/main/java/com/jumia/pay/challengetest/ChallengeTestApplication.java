package com.jumia.pay.challengetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "com.jumia.pay.challengetest.repository")
@EnableTransactionManagement
@SpringBootApplication
@ComponentScan(basePackages = {"com.jumia.pay.challengetest"})
public class ChallengeTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChallengeTestApplication.class, args);
    }

}

