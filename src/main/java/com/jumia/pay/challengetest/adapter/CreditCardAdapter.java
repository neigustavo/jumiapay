package com.jumia.pay.challengetest.adapter;

import com.jumia.pay.challengetest.entity.CreditCardStat;
import com.jumia.pay.challengetest.integration.obj.Bank;
import com.jumia.pay.challengetest.integration.obj.CreditCard;
import com.jumia.pay.challengetest.view.CardStatsResponse;
import com.jumia.pay.challengetest.view.CreditCardDetailResponse;
import com.jumia.pay.challengetest.view.CreditCardInfoResponse;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CreditCardAdapter {

    public CreditCardInfoResponse adaptToCreditCardInfoResponse(CreditCard card) {
        CreditCardDetailResponse detail = new CreditCardDetailResponse();
        Bank bank = card.getBank();
        if(bank != null) {
            detail.setBank(bank.getName());
        }
        detail.setScheme(card.getScheme());
        detail.setType(card.getType());
        return new CreditCardInfoResponse(detail);
    }

    public CreditCardStat adaptToCreditCardStat(String cardId) {
        CreditCardStat stat = new CreditCardStat();
        stat.setCount(1);
        stat.setCardNumber(Long.valueOf(cardId));
        return stat;
    }

    public CardStatsResponse adaptToCardStatsResponse(List<CreditCardStat> creditCardStats, int start, int limit) {
        if(!creditCardStats.isEmpty()) {
            Map<String, String>  cardStats = new HashMap<>();
            creditCardStats.forEach(cardStat -> cardStats.put(String.valueOf(cardStat.getCardNumber()), String.valueOf(cardStat.getCount())));
            return  new CardStatsResponse(start, limit, creditCardStats.size(), cardStats);
        }
        return null;
    }
}
