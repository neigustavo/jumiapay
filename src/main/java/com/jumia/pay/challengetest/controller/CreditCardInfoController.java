package com.jumia.pay.challengetest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jumia.pay.challengetest.service.CreditCardService;
import com.jumia.pay.challengetest.util.BeanFactory;
import com.jumia.pay.challengetest.view.CardStatsResponse;
import com.jumia.pay.challengetest.view.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class CreditCardInfoController {

    @Autowired
    private CreditCardService service;

    @Autowired
    private ObjectMapper objectMapper;


    @RequestMapping(value = "/card-scheme/verify/{cardId}", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String findCreditCardInformation(@PathVariable String cardId) {
        try {
            return objectMapper.writeValueAsString(service.findCreditCardInformation(cardId));
        } catch (Exception e) {
            try {
                return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
            } catch (JsonProcessingException e1) {
                return new String("{success=false, {payload=\"" + e1.getMessage() + "\"}}");
            }
        }
    }

    @RequestMapping(value = "/card-scheme/stats", method = RequestMethod.GET, consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getStats(@RequestParam("start") int start, @RequestParam("limit") int limit) {
        try {
            return objectMapper.writeValueAsString(service.findStatsBetween(start, limit));
        } catch (Exception e) {
            try {
                return objectMapper.writeValueAsString(new ErrorResponse(e.getMessage()));
            } catch (JsonProcessingException e1) {
                return new String("{success=false, {payload=\"" + e1.getMessage() + "\"}}");
            }
        }
    }
}
