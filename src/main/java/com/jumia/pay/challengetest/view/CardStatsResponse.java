package com.jumia.pay.challengetest.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardStatsResponse {

    @JsonProperty("success")
    private boolean success;

    @JsonProperty("start")
    private int start;

    @JsonProperty("limit")
    private int limit;

    @JsonProperty("size")
    private long size;

    @JsonProperty("payload")
    private Map<String, String> payload;

    public CardStatsResponse(int start, int limit, long size, Map<String, String> payload) {
        this.success = true;
        this.start = start;
        this.limit = limit;
        this.size = size;
        this.payload = payload;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Map<String, String> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, String> payload) {
        this.payload = payload;
    }
}
