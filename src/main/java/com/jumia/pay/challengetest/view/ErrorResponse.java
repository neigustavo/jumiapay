package com.jumia.pay.challengetest.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {

    @JsonProperty("success")
    private boolean success;

    @JsonProperty("payload")
    private ErrorPayload paylaod;

    public ErrorResponse(String message) {
        this.success = false;
        this.paylaod = new ErrorPayload(message);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ErrorPayload getPaylaod() {
        return paylaod;
    }

    public void setPaylaod(ErrorPayload paylaod) {
        this.paylaod = paylaod;
    }
}
