package com.jumia.pay.challengetest.service;

import com.jumia.pay.challengetest.adapter.CreditCardAdapter;
import com.jumia.pay.challengetest.entity.CreditCardStat;
import com.jumia.pay.challengetest.entity.PageableStat;
import com.jumia.pay.challengetest.integration.CreditCardIntegration;
import com.jumia.pay.challengetest.integration.obj.CreditCard;
import com.jumia.pay.challengetest.repository.CreditCardStatRepository;
import com.jumia.pay.challengetest.repository.CreditCardStatService;
import com.jumia.pay.challengetest.view.CardStatsResponse;
import com.jumia.pay.challengetest.view.CreditCardInfoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CreditCardIntegration integration;

    @Autowired
    private CreditCardAdapter adapter;

    @Autowired
    private CreditCardStatService statService;

    @Resource
    private CreditCardStatRepository creditCardStatRepo;



    public CreditCardInfoResponse findCreditCardInformation(String cardId) throws IOException {
        CreditCard cardInformation = integration.findCreditCardInformation(cardId);
        saveStats(cardId);
        return adapter.adaptToCreditCardInfoResponse(cardInformation);
    }

    public CardStatsResponse findStatsBetween(int start, int limit) {
        List<CreditCardStat> all = statService.findAll(start, limit);
        return adapter.adaptToCardStatsResponse(all, start, limit);
    }

    private void saveStats(String cardId) {
        Optional<CreditCardStat> opt = creditCardStatRepo.findById(Long.valueOf(cardId));
        CreditCardStat stat = null;
        if(opt.isPresent()) {
            stat = opt.get();
            stat.setCount(stat.getCount()+1);
        } else {
            stat = adapter.adaptToCreditCardStat(cardId);
        }
        creditCardStatRepo.save(stat);
    }



}
